<?php namespace Routes;

use Core\Router;

$routes = new Router();
/**
 * Routes List
 **/
$routes->add("/", "HomeController@index");
$routes->add("api/posts", "Api\PostController@all");
$routes->add("api/post/{id}", "Api\PostController@post");
$routes->add("api/categories", "Api\CategoryController@all");
$routes->add("api/login", "Api\UserController@login");
$routes->add("api/user", "Api\UserController@user");
$routes->add("api/logout", "Api\UserController@logout");
$routes->add("api/post/delete/{id}", "Api\PostController@delete");
$routes->add("api/posts/create", "Api\PostController@create");
$routes->add("api/posts/update/{id}", "Api\PostController@update");
$routes->add("api/register", "Api\UserController@register");
/**
 * Convert Api - spell out
 */
$routes->add("api/convert", "Api\ConvertController@convert");
/**
 * End Routes List
 **/
return $routes;