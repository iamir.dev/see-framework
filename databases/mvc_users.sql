INSERT INTO mvc.users (id, name, email, type, pass, token, created_at, updated_at)
VALUES (11, 'Amir Abbaspour', 'a.h.edalati@hotmail.com', 'Member',
        '$2y$10$zHJ.yxTprtncTiscND3UJerNo1N5uuYp9Sq0BMl7x8UjzxVvmo3i6', null, '2019-07-18 07:06:49',
        '2019-07-18 07:06:49');
INSERT INTO mvc.users (id, name, email, type, pass, token, created_at, updated_at)
VALUES (12, 'Demo', 'demo@demo.com', 'Member', '$2y$10$ochXFUHLK/2fSWvezf0pJOrpS2xnMLRaGu3z1vZuYXabtT.hZqlGW', null,
        '2019-07-19 01:49:34', '2019-07-19 01:49:34');