<?php

use Core\Router;

require_once __DIR__ . '/../vendor/autoload.php';

//error_reporting(E_ALL);
//set_error_handler('Core\Error::errorHandler');
//set_exception_handler('Core\Error::exceptionHandler');

$routes = require __DIR__ . '/../routes/routes.php';

$routes->run(Router::url());