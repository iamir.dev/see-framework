<?php


namespace App\Controllers\Api;


use App\Models\User;
use Carbon\Carbon;
use Core\Controller;
use Core\helpers\JWT_API;
use Core\helpers\Req;
use Exception;

class UserController extends Controller
{
    protected $user_model;
    protected $auth_errors = [];
    protected $register_errors = [];

    public function __construct()
    {
        $this->user_model = new User();
    }

    public function login()
    {

        if (Req::is_post()) {
            try {
                $login_play_load = Req::play_load();
                $user_exist = $this->user_model->find('email', $login_play_load['email']);
                if ($user_exist === false)
                    $this->auth_errors[] = 'User not exist';
                else {
                    $login_check = password_verify($login_play_load['password'], $user_exist->pass);
                    if (!$login_check)
                        $this->auth_errors[] = 'Password is not correct';
                    else {

                        $login_jwt = $this->set_login($user_exist);
                        $login_jwt_response = [
                            'data' => [
                                'id' => $user_exist->id,
                                'email' => $user_exist->email,
                                'name' => $user_exist->name
                            ],
                            'meta' => [
                                'token' => $login_jwt['token']
                            ]
                        ];
                        $this->json_api($login_jwt_response);
                    }
                }
            } catch (Exception $e) {
                $this->auth_errors[] = $e;
            }
            if (!empty($this->auth_errors))
                $this->json_api_errors($this->auth_errors);

        }
    }

    public function set_login($user_exist)
    {
        $remember = false;
        if (!empty(Req::input('remember')))
            $remember = true;
        return JWT_API::login($user_exist, $remember);

    }

    public function user()
    {
        $JWT = JWT_API::validate();
        $user_details = $this->user_model->find("id", $JWT["user_id"]);
        /**
         * Remove pass from return
         */
        unset($user_details->pass);
        $this->json_api(["isLoggedIn" => $JWT["verify"], "data" => $user_details]);
    }

    public function logout()
    {
        $this->json_api(["isLoggedIn" => false]);
    }

    public function register()
    {
        if (Req::is_post()) {
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique.users',
                'password' => 'required',
                'confirm' => 'required|confirm.password'
            ];

            try {
                $register_play_load = Req::play_load();
                $validation = $this->valid($rules, true);
                if ($validation['result'] === true) {
                    $register = $this->user_model->create([
                        'name' => $register_play_load["name"],
                        'email' => $register_play_load["email"],
                        'pass' => password_hash($register_play_load["password"], PASSWORD_BCRYPT),
                        'type' => 'Member',
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                    if (!$register) {
                        $this->register_errors = "Unable to register , please try again.";
                    } else {
                        $this->json_api(["is_registered" => $register, "data" => $register_play_load]);
                    }
                } else {
                    $this->register_errors = $validation["errors"];
                }
            } catch (Exception $e) {
                $this->register_errors = $e;
            }

        }

        if (!empty($this->register_errors))
            $this->json_api_errors($this->register_errors);
    }
}