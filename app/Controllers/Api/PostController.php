<?php


namespace App\Controllers\Api;


use App\Models\Post;
use Carbon\Carbon;
use Core\Controller;
use Core\helpers\JWT_API;
use Core\helpers\Req;
use Exception;

class PostController extends Controller
{
    protected $post_model;

    public function __construct()
    {
        $this->post_model = new Post();
    }

    public function all()
    {
        $all_posts = $this->post_model->all();
        $this->json_api($all_posts);
    }

    public function post($id)
    {
        $post = $this->post_model->find("id", $id);
        $this->json_api($post);
    }

    public function delete($id)
    {
        $JWT = JWT_API::validate();
        if ($JWT["verify"] === true) {
            $this->post_model->delete($id)->get();
            $this->json_api(['deleted' => true, "isLoggedIn" => $JWT["verify"]]);
        } else {
            $error = ["Token is not valid please logout and login again !"];
            $this->json_api_errors($error);
        }
    }

    public function create()
    {
        if (Req::is_post()) {
            $JWT = JWT_API::validate();
            if ($JWT["verify"] === true) {
                try {
                    $post_play_load = Req::play_load();
                    $create = $this->post_model->create([
                        "title" => $post_play_load["title"],
                        "slug" => $post_play_load["slug"],
                        "body" => $post_play_load["body"],
                        "description" => $post_play_load["description"],
                        "category_id" => $post_play_load["category"],
                        "user_id" => $JWT["user_id"],
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    $this->json_api(["is_created" => $create, "post_id" => $this->post_model->last_id()]);
                } catch (Exception $e) {
                    $error = ["Post route !!!!"];
                    $this->json_api_errors($error);
                }
            } else {
                $error = ["Token is not valid please logout and login again !"];
                $this->json_api_errors($error);
            }
        } else {
            $error = ["Post route !!!!"];
            $this->json_api_errors($error);
        }
    }

    public function update($id)
    {
        if (Req::is_post()) {
            $JWT = JWT_API::validate();
            if ($JWT["verify"] === true) {
                try {
                    $post_play_load = Req::play_load();
                    $post_id = $id;
                    $update = $this->post_model->update($post_id, [
                        "title" => $post_play_load["title"],
                        "slug" => $post_play_load["slug"],
                        "category_id" => $post_play_load["category"],
                        "body" => $post_play_load["body"],
                        "description" => $post_play_load["description"],
                        "user_id" => $JWT["user_id"],
                        "updated_at" => Carbon::now()
                    ]);
                    $this->json_api(["is_updated" => $update, "post_id" => $post_id]);
                } catch (Exception $e) {
                    $error = ["Post update error !!!!!!"];
                    $this->json_api_errors($error);
                }
            }
        }
    }
}