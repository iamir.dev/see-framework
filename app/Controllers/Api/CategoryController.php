<?php


namespace App\Controllers\Api;


use App\Models\Category;
use Core\Controller;

class CategoryController extends Controller
{
    protected $category_model;

    public function __construct()
    {
        $this->category_model = new Category();
    }

    public function all()
    {
        $all_categories = $this->category_model->all();
        $this->json_api($all_categories);
    }
}
