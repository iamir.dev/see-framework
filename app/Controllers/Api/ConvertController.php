<?php

namespace App\Controllers\Api;

use Core\Controller;
use Core\helpers\Req;
use Exception;
use Salarmehr\Cosmopolitan\Cosmo;

/**
 * Convert number - to other locales and spell out
 */
class ConvertController extends Controller
{
    /**
     * Convert - spell out number
     */
    public function convert()
    {
        if (Req::is_post()) {
            try {
                $post_play_load = Req::play_load();
                $language = $post_play_load["language"];
                $number = $post_play_load["number"];
                $spellOut = Cosmo::create($language)->spellout($number);
                $this->json_api(["spellout" => $spellOut]);
            } catch (Exception $e) {
                $error = ["Convert error !!!!!!"];
                $this->json_api_errors($error);
            }
        }
    }
}