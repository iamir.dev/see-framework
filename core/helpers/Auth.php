<?php

namespace Core\helpers;

use Core\contracts\authInterface;
use Core\DB;

/**
 * Class Auth
 * @package Core\Helpers
 */
class Auth implements authInterface
{

    /**
     * @param $user
     * @param bool $remember
     */
    public static function login($user, $remember = false)
    {
        Session::set('email', $user->email);
        if ($remember === true) {
            $token = generate_random_string();
            (new DB())->from('users')->update($user->id, [
                'token' => $token
            ]);
            Cookie::set('token', $token);
        }
    }


    /**
     * @return bool
     */
    public static function check()
    {
        if (Session::exists('email')) {
            $email = Session::get('email');
            $user = (new DB())->from('users')->find('email', $email);
            if (!$user) {
                Session::forget('email');
                return false;
            }
            return true;
        } elseif (Cookie::exists('token')) {
            $token = Cookie::get('token');
            $user = (new DB())->from('users')->find('token', $token);
            if (!$user) {
                Cookie::forget('token');
                return false;
            }
            Session::set('email', $user->email);
            return true;
        }
        return false;
    }


    /**
     *
     */
    public static function logout()
    {
        if (Cookie::exists('token')) {
            Cookie::forget('token');
            Session::forget('email');
        }

        if (Session::exists('email')) {
            Session::forget('email');
        }

        redirect();
    }


    /**
     * @return mixed
     */
    public static function user()
    {
        $session_email = Session::get('email');
        return (new DB())->from('users')->find('email', $session_email);
    }
}