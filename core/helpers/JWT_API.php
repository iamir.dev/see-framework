<?php


namespace Core\helpers;


use Core\contracts\jwtInterface;

class JWT_API implements jwtInterface
{

    /**
     * @param $user_exist
     * @param $remember
     * @return mixed
     */
    public static function login($user_exist, $remember)
    {
        $URL = configs()['URL'];
        $API_URL = configs()['API_URL'];
        $user_id = $user_exist->id;
        $time = time();
        $token = (new Builder())->issuedBy($URL)// Configures the issuer (iss claim)
        ->permittedFor($API_URL)// Configures the audience (aud claim)
        ->identifiedBy('aaru18j1f1v', true)// Configures the id (jti claim), replicating as a header item
        ->issuedAt($time)// Configures the time that the token was issue (iat claim)
        ->canOnlyBeUsedAfter($time /*+ 60*/)// Configures the time that the token can be used (nbf claim)
        ->expiresAt($time + 3600000)// Configures the expiration time of the token (exp claim)
        ->withClaim('uid', $user_id)// Configures a new claim, called "uid"
        ->getToken(); // Retrieves the generated token

        $token->getHeaders(); // Retrieves the token headers
        $token->getClaims(); // Retrieves the token claims

        return [
            'jti' => $token->getHeader('jti'),
            'iss' => $token->getClaim('iss'),
            'uid' => $token->getClaim('uid'),
            'token' => $token->getPayload() . '.'
        ];

    }

    public static function validate()
    {
        $URL = configs()['URL'];
        $API_URL = configs()['API_URL'];
        $token_string = $_SERVER["HTTP_AUTHORIZATION"];
        $token_string = preg_replace("/(Bearer\s)/", "", $token_string);
        $token = (new Parser())->parse((string)$token_string);
        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer($URL);
        $data->setAudience($API_URL);
        $data->setId('aaru18j1f1v');
        $user_id = $token->getClaim('uid');
        return [
            "verify" => $token->validate($data),
            "user_id" => $user_id
        ];
    }
}