<?php

namespace Core\helpers;

use Core\contracts\scInterface;

/**
 * Class Session
 * @package Core\Helpers
 */
class Session implements scInterface
{

    /**
     * @param $key
     * @return bool
     */
    public static function exists($key)
    {
        return array_key_exists($key, $_SESSION);
    }


    /**
     * @param $key
     * @return bool|mixed
     */
    public static function get($key)
    {
        return self::exists($key) ? $_SESSION[$key] : false;
    }


    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }


    /**
     * @param $key
     */
    public static function forget($key)
    {
        unset($_SESSION[$key]);
    }
}