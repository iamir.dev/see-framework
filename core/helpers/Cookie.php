<?php

namespace Core\helpers;

use Core\contracts\scInterface;

/**
 * Class Cookie
 * @package Core\Helpers
 */
class Cookie implements scInterface
{


    /**
     * @param $key
     * @return bool
     */
    public static function exists($key)
    {
        return array_key_exists($key, $_COOKIE);
    }


    /**
     * @param $key
     * @return bool|mixed
     */
    public static function get($key)
    {
        return self::exists($key) ? $_COOKIE[$key] : false;
    }


    /**
     * @param $key
     * @param $value
     * @param string $time
     */
    public static function set($key, $value, $time = "+30 day")
    {
        setcookie($key, $value, strtotime($time));
    }


    /**
     * @param $key
     */
    public static function forget($key)
    {
        setcookie($key, "", strtotime("-5 day"), "/");
    }
}