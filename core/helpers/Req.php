<?php

namespace Core\helpers;

use Core\contracts\requestInterface;
use Exception;

/**
 * Class Req
 * @package Core\Helpers
 */
class Req implements requestInterface
{

    /**
     * @param $field_name
     * @param bool $is_post
     * @return string|null
     */
    public static function input($field_name, $is_post = true)
    {
        if (self::is_post() && $is_post)
            return isset($_POST[$field_name]) ? htmlspecialchars($_POST[$field_name]) : null;
        return isset($_GET[$field_name]) ? htmlspecialchars($_GET[$field_name]) : null;
    }


    /**
     * @param bool $is_post
     * @return array|null
     */
    public static function all($is_post = true)
    {
        if (self::is_post() && $is_post)
            return isset($_POST) ? array_map('htmlspecialchars', $_POST) : null;
        return isset($_GET) ? array_map('htmlspecialchars', $_GET) : null;
    }


    /**
     * @return bool
     */
    public static function is_post()
    {
        return ($_SERVER['REQUEST_METHOD'] === 'POST') ? true : false;
    }

    /**
     * @return mixed|void
     * @throws Exception
     */
    public static function play_load()
    {

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
            throw new Exception('Request method must be POST!');
        }
        //Make sure that the content type of the POST request has been set to application/json
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if (strpos($contentType, 'application/json') === false) {
            throw new Exception('Content type must be: application/json');
        }

        //Receive the RAW post data.
        $content = trim(file_get_contents("php://input"));

        //Attempt to decode the incoming RAW post data from JSON.
        $decoded = json_decode($content, true);

        //If json_decode failed, the JSON is invalid.
        if (!is_array($decoded)) {
            throw new Exception('Received content contained invalid JSON!');
        }

        return $decoded;

    }
}