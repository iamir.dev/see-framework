<?php

namespace Core\helpers;


use Core\contracts\validationInterface;
use Core\DB;

/**
 * Class Validation
 * @package Core\Helpers
 */
class Validation implements validationInterface
{
    /**
     * @var
     */
    private $errors;
    /**
     * @var
     */
    private $data;


    /**
     * @param $data
     * @param $rules
     * @return bool|mixed
     */
    public function valid($data, $rules)
    {
        $is_valid = true;
        $this->data = $data;
        foreach ($rules as $rules_key => $rules_base) {
            $rules = explode('|', $rules_base);
            foreach ($rules as $rule) {
                $rule_param = null;
                if (strpos($rule, '.') !== false) {
                    $param_rule = explode('.', $rule);
                    $rule_param = $param_rule[1];
                    $rule = $param_rule[0];
                }
                $data_val = isset($data[$rules_key]) ? $data[$rules_key] : null;
                if (method_exists($this, $rule)) {
                    if ($this->{$rule}($rules_key, $data_val, $rule_param) === false) {
                        $is_valid = false;
                        break;
                    }
                }
            }
        }

        return $is_valid;
    }


    /**
     * @param $rules_key
     * @param $data_val
     * @return bool|mixed
     */
    public function required($rules_key, $data_val)
    {
        if (!empty($data_val)) {
            return true;
        } else {
            $field_name = ucfirst($rules_key);
            $this->errors[$rules_key][] = "{$field_name} field is required !";
            return false;
        }
    }


    /**
     * @param $rules_key
     * @param $data_val
     * @return bool|mixed
     */
    public function email($rules_key, $data_val)
    {
        if (!filter_var($data_val, FILTER_VALIDATE_EMAIL)) {
            $this->errors[$rules_key][] = "Email field is not validate !";
            return false;

        } else {
            return true;
        }
    }


    /**
     * @return mixed
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * @param $rules_key
     * @param $data_val
     * @param $rule_param
     * @return bool|mixed
     */
    public function confirm($rules_key, $data_val, $rule_param)
    {
        $confirm_val = isset($this->data[$rule_param]) ? $this->data[$rule_param] : null;
        if ($confirm_val !== $data_val) {
            $field_name = ucfirst($rules_key);
            $confirm_field_name = ucfirst($rule_param);
            $this->errors[$rules_key][] = "{$field_name} is not equal with {$confirm_field_name}";
            return false;
        } else {
            return true;
        }
    }


    /**
     * @param $rules_key
     * @param $data_val
     * @param $rule_param
     * @return bool|mixed
     */
    public function unique($rules_key, $data_val, $rule_param)
    {
        if (is_null($rule_param))
            return false;
        $DB = new DB;
        if ($DB->from($rule_param)->find($rules_key, $data_val) !== false) {
            $field_name = ucfirst($rules_key);
            $this->errors[$rules_key][] = "{$field_name} is not unique !";
            return false;
        }
        return true;

    }
}