<?php

use Core\helpers\Auth;
use Core\helpers\Req;

/**
 * @param $field_name
 * @return string|null
 */
function previous_value($field_name)
{
    $Req = Req::input($field_name);
    return isset($Req) ? $Req : null;
}


/**
 * @param null $path
 */
function redirect($path = null)
{
    if (is_null($path))
        $path = "/";
    header("location:{$path}");
    exit();

}


/**
 * @param int $length
 * @return string
 */
function generate_random_string($length = 100)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


/**
 * @return bool
 */
function is_login()
{
    return Auth::check() ? true : false;
}

/**
 * @return mixed
 */
function configs()
{
    $configs = require __DIR__ . '/../configs.php';
    return $configs;
}