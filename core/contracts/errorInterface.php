<?php


namespace Core\contracts;


/**
 * Interface errorInterface
 * @package Core\Contracts
 */
interface errorInterface
{
    /**
     * @param $level
     * @param $message
     * @param $file
     * @param $line
     * @return mixed
     */
    public static function errorHandler($level, $message, $file, $line);

    /**
     * @param $exception
     * @return mixed
     */
    public static function exceptionHandler($exception);
}