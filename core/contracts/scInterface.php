<?php

namespace Core\contracts;

/**
 * Interface scInterface
 * @package Core\Contracts
 */
interface scInterface
{

    /**
     * @param $key
     * @return mixed
     */
    public static function exists($key);


    /**
     * @param $key
     * @return mixed
     */
    public static function get($key);


    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set($key, $value);


    /**
     * @param $key
     * @return mixed
     */
    public static function forget($key);
}