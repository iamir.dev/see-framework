<?php


namespace Core\contracts;


/**
 * Interface viewInterface
 * @package Core\Contracts
 */
interface viewInterface
{
    /**
     * @param $view
     * @param array $args
     * @return mixed
     */
    public static function render($view, $args = []);
}