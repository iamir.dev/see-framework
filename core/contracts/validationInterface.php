<?php


namespace Core\contracts;


/**
 * Interface validationInterface
 * @package Core\Contracts
 */
interface validationInterface
{

    /**
     * @param $data
     * @param $rules
     * @return mixed
     */
    public function valid($data, $rules);


    /**
     * @param $rules_key
     * @param $data_val
     * @return mixed
     */
    public function required($rules_key, $data_val);


    /**
     * @param $rules_key
     * @param $data_val
     * @return mixed
     */
    public function email($rules_key, $data_val);


    /**
     * @return mixed
     */
    public function errors();


    /**
     * @param $rules_key
     * @param $data_val
     * @param $rule_param
     * @return mixed
     */
    public function confirm($rules_key, $data_val, $rule_param);


    /**
     * @param $rules_key
     * @param $data_val
     * @param $rule_param
     * @return mixed
     */
    public function unique($rules_key, $data_val, $rule_param);
}