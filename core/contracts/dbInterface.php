<?php

namespace Core\contracts;

/**
 * Interface dbInterface
 * @package Core\Contracts
 */
interface dbInterface
{

    /**
     * @param $table
     * @return mixed
     */
    public function from($table);


    /**
     * @param array $selections_array
     * @return mixed
     */
    public function select($selections_array = []);


    /**
     * @param $field
     * @param $value
     * @param string $operator
     * @return mixed
     */
    public function where($field, $value, $operator = "=");


    /**
     * @param $data
     * @return mixed
     */
    public function create($data);


    /**
     * @return mixed
     */
    public function sql();


    /**
     * @return mixed
     */
    public function run();


    /**
     * @return mixed
     */
    public function get();


    /**
     * @return mixed
     */
    public function all();


    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function find($field, $value);


    /**
     * @return mixed
     */
    public function first();


    /**
     * @param $limit
     * @return mixed
     */
    public function limit($limit);


    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @return mixed
     */
    public function last_id();

}