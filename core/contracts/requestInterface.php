<?php

namespace Core\contracts;


/**
 * Interface requestInterface
 * @package Core\Contracts
 */
interface requestInterface
{


    /**
     * @param $field_name
     * @param bool $is_post
     * @return mixed
     */
    public static function input($field_name, $is_post = true);


    /**
     * @param bool $is_post
     * @return mixed
     */
    public static function all($is_post = true);


    /**
     * @return mixed
     */
    public static function is_post();

    /**
     * @return mixed
     */
    public static function play_load();
}