<?php


namespace Core\contracts;


/**
 * Interface jwtInterface
 * @package Core\contracts
 */
interface jwtInterface
{
    /**
     * @param $user_exist
     * @param $remember
     * @return mixed
     */
    public static function login($user_exist, $remember);

    /**
     * @return mixed
     */
    public static function validate();
}