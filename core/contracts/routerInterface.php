<?php


namespace Core\contracts;


/**
 * Interface routerInterface
 * @package Core\Contracts
 */
interface routerInterface
{
    /**
     * @param $route
     * @param $parameters
     * @return mixed
     */
    public function add($route, $parameters);

    /**
     * @return mixed
     */
    public function routes();

    /**
     * @param $url
     * @return mixed
     */
    public function find($url);

    /**
     * @return mixed
     */
    public static function url();

    /**
     * @return mixed
     */
    public function parameters();

    /**
     * @param $url
     * @return mixed
     */
    public function run($url);
}