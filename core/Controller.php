<?php


namespace Core;


use Core\contracts\controllerInterface;
use Core\helpers\Req;
use Core\helpers\Validation;
use Exception;

/**
 * Class Controller
 * @package Core
 */
class Controller implements controllerInterface
{
    protected function json_api($return_object)
    {
        $json = json_encode($return_object);
        header('Content-Type: application/json');
        echo $json;
    }

    protected function Valid($rules, $is_play_load = false)
    {
        $validation = new Validation();
        if (!$is_play_load) {
            $make_validation = $validation->valid(Req::all(), $rules);
        } else {
            try {
                $make_validation = $validation->valid(Req::play_load(), $rules);
            } catch (Exception $e) {
                return ['result' => false, 'data' => null, 'errors' => "Json post error !!!"];
            }
        }
        if ($make_validation) {
            return ['result' => true, 'data' => $make_validation, 'errors' => null];
        }
        foreach ($validation->errors() as $errors) {
            return ['result' => false, 'data' => null, 'errors' => $errors];
        }
    }


    protected function json_api_errors($errors)
    {
        http_response_code(500);
        $this->json_api($errors);

    }
}