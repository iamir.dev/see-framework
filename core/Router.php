<?php


namespace Core;


use Core\contracts\routerInterface;
use Exception;

/**
 * Class Router
 * @package Core
 */
class Router implements routerInterface
{
    /**
     * @var array
     */
    protected $routes = [];
    /**
     * @var array
     */
    protected $parameters = [];
    /**
     * @var string
     */
    protected $namespace = "App\Controllers\\";
    /**
     * @var array
     */
    protected $route_params = [];

    /**
     * @param $route
     * @param $parameters
     */
    public function add($route, $parameters)
    {
        $route = preg_replace('/\/$/', '', $route);
        $route = preg_replace('/\//', '\/', $route);
        $route_pattern = preg_replace('/\{([a-z]+)\}/', '(?<\\1>[a-z0-9-]+)', $route);
        $methods_controllers = is_array($parameters) ? $parameters["controller"] : $parameters;
        /**
         * List Array list()
         */
        list($route_parameters['controller'], $route_parameters['method']) = explode("@", $methods_controllers);

        $this->routes[$route_pattern] = $route_parameters;
    }

    /**
     * @return array
     */
    public function routes()
    {
        return $this->routes;
    }

    /**
     * @param $url
     * @return bool
     */
    public function find($url)
    {
        $url = $this->clean_query_variables($url);
        foreach ($this->routes as $route_pattern => $params) {
            if (preg_match('/^\/' . $route_pattern . '\/?$/i', $url, $match_routes)) {
                foreach ($match_routes as $match_route_param => $match_route) {
                    if (is_string($match_route_param)) {
                        $this->parameters[$match_route_param] = $match_route;

                    }

                }
                $this->route_params = $params;
                return true;
            }
        }
        return false;
    }

    /**
     * @return array|mixed
     */
    public static function url()
    {
        $url = $_SERVER['REQUEST_URI'];
        if (filter_var($url, FILTER_SANITIZE_URL))
            return $url;
        return [];
    }

    /**
     * @return array
     */
    public function parameters()
    {
        return $this->parameters;
    }

    /**
     * @param $url
     * @return mixed
     * @throws Exception
     */
    public function run($url)
    {

        if ($this->find($url)) {
            $controller = $this->namespace . $this->route_params['controller'];
            if (class_exists($controller)) {

                $controller_object = new $controller();
                $controller_method = $this->route_params['method'];
                if (is_callable([$controller_object, $controller_method])) {
                    return call_user_func_array([$controller_object, $controller_method], $this->parameters);
                } else {
                    throw new Exception("Method {$controller_method} (in controller {$controller}) not found");
                }
            } else {
                throw new Exception("Controller class {$controller} not found");
            }
        } else {
            throw new Exception("no route matched.", 404);
        }
    }

    /**
     * @param $url
     * @return string
     */
    protected function clean_query_variables($url)
    {
        if (!empty($url)) {
            $parts = explode("?", $url, 2);
            if (strpos($parts[0], '=') === false) {
                $url = $parts[0];
            } else {
                $url = "";
            }
            return $url;
        }

        return null;
    }

}