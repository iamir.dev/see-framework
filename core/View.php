<?php namespace Core;

use Core\contracts\viewInterface;
use Exception;

/**
 * Class View
 * @package Core
 */
class View implements viewInterface
{
    /**
     * @param $view
     * @param array $args
     * @throws Exception
     */
    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);

        $file = __DIR__ . "/../resources/views/{$view}.php";

        if (is_readable($file)) {
            require $file;
        } else {
            throw new Exception("{$file} not found");
        }

    }
}