<?php

namespace Core;

use Core\contracts\dbInterface;
use Exception;
use PDO;

/**
 * Class DB
 * @package Core
 */
class DB implements dbInterface
{
    /**
     * @var
     */
    protected $delete = null;
    /**
     * @var PDO
     */
    protected $mysql;
    /**
     * @var
     */
    protected $table;
    /**
     * @var int
     */
    protected $fetch_mode = PDO::FETCH_OBJ;
    /**
     * @var string
     */
    protected $fetch_type = 'fetchAll';
    /**
     * @var
     */
    protected $statement;
    /**
     * @var array
     */
    protected $bind_values = [];
    /**
     * @var array
     */
    protected $selections = [];
    /**
     * @var array
     */
    protected $wheres = [];
    /**
     * @var array
     */
    protected $or_wheres = [];
    /**
     * @var
     */
    protected $limit;

    /**
     * @var
     */
    protected $last_id;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $configs = configs();

        try {
            $this->mysql = new PDO("mysql:host={$configs['DB']['HOST']};dbname={$configs['DB']['NAME']}", $configs['DB']['USER'], $configs['DB']['PASS']);
        } catch (Exception $exception) {
            //Err
        }

    }

    /**
     * @param $table
     * @return $this
     */
    public function from($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @param array $selections_array
     * @return $this
     */
    public function select($selections_array = [])
    {
        /**
         * func_get_args();
         */

        $this->selections = array_values($selections_array);
        return $this;
    }

    /**
     * @param $field
     * @param $value
     * @param string $operator
     * @return $this
     */
    public function where($field, $value, $operator = "=")
    {
        $this->wheres[] = "$field $operator :$field";
        if ($operator === "LIKE")
            $this->bind_values[$field] = "% {$value} %";
        $this->bind_values[$field] = $value;
        return $this;
    }

    /**
     * @param $data
     * @return bool
     */
    public function create($data)
    {
        $fields = join(', ', array_keys($data));
        $params = " :" . join(', :', array_keys($data));
        $this->statement = $this->mysql->prepare("INSERT INTO {$this->table} ($fields) VALUES ($params)");
        $this->bind_values = $data;
        $this->binding();
        $is_created = $this->statement->execute();
        $this->last_id = $this->mysql->lastInsertId();
        return $is_created;
    }

    /**
     *
     */
    private function binding()
    {
        foreach ($this->bind_values as $bind_key => $bind_value)
            $this->statement->bindValue(":$bind_key", $bind_value);
    }

    /**
     * @return string
     */
    public function sql()
    {
        if (!is_null($this->delete))
            $query[] = $this->delete;
        elseif (empty($this->selections)) {
            $query[] = "SELECT * FROM {$this->table}";
        } else {
            $join_selections = join(', ', $this->selections);
            $query[] = "SELECT {$join_selections} FROM {$this->table}";
        }
        if (!empty($this->wheres)) {
            $where_query = "WHERE " . join(" AND ", $this->wheres);
            $query[] = $where_query;
            if (!empty($this->or_wheres)) {
                $or_where_query = " OR " . join(" OR ", $this->wheres);
                $query[] = $or_where_query;
            }
        }
        if (is_null($this->delete)) {

            if (!empty($this->limit))
                $query[] = "LIMIT {$this->limit}";
        }
        return join(" ", $query);
    }

    /**
     * @return $this
     */
    public function run()
    {
        $this->statement = $this->mysql->prepare($this->sql());
        $this->binding();
        $this->statement->execute();
        return $this;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $this->run();
        return $this->fetch();
    }

    /**
     * @return mixed
     */
    private function fetch()
    {
        return $this->statement->{($this->fetch_type == 'fetchAll') ? 'fetchAll' : 'fetch'}($this->fetch_mode);
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->select()->get();
    }

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function find($field, $value)
    {
        return $this->select()->where($field, $value)->first();
    }

    /**
     * @return mixed
     */
    public function first()
    {
        $this->limit(1)->run();
        $this->fetch_type = 'fetch';
        return $this->fetch();
    }

    /**
     * @param $limit
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }


    /**
     * @param $id
     * @param $data
     * @return bool
     */
    public function update($id, $data)
    {
        $object = $this->find('id', $id);
        if (!$object)
            return false;

        $update_fields = $this->update_fields($data);
        $this->statement = $this->mysql->prepare("UPDATE {$this->table} SET {$update_fields} WHERE id = :id");
        $this->bind_values = array_merge($data, ['id' => $id]);
        $this->binding();

        return $this->statement->execute();
    }

    /**
     * @param $data
     * @return string
     */
    private function update_fields($data)
    {
        $field = [];
        foreach (array_keys($data) as $name) {
            $field[] = "$name = :$name";
        }
        return join(', ', $field);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $object = $this->find('id', $id);
        if (!$object)
            return false;
        $this->delete = "DELETE FROM {$this->table} ";
        return $this;
    }

    /**
     * @return mixed
     */
    public function last_id()
    {
        return $this->last_id;
    }
}